import uuid
import random
import gevent
from locust import SequentialTaskSet, task, FastHttpUser, constant_throughput

class UsersPool:
    registered_users = set()
    users_with_post = set()
    available_tokens = set()
    available_post_uid_token = set()
    post_uids = set()

    def get_register_details(self):
        external_user_uid = str(uuid.uuid4())
        username = f"t_{external_user_uid[:-2]}"
        email = f"{uuid.uuid4()}@kapitest.bar"
        return external_user_uid, username, email
    
    def add_to_registered_user(self, username):
        self.registered_users.add(username)

    def get_registered_user(self):
        if self.registered_users:
            return self.registered_users.pop()
        
    def add_to_tokens(self, token):
        self.available_tokens.add(token)

    def get_token(self):
        if self.available_tokens:
            return self.available_tokens.pop()
        
    def add_to_post_uid_tokens(self, uid):
        self.available_post_uid_token.add(uid)

    def get_post_uid_token(self):
        if self.available_post_uid_token:
            return self.available_post_uid_token.pop()
        return None, None
    
    def add_to_post_uids(self, uid):
        self.post_uids.add(uid)

    def get_post_uids(self):
        return self.post_uids
        

users_pool = UsersPool()

class RegisterAndPublishPost(SequentialTaskSet):
    def on_start(self):
        self.internal_access_headers = {"X-Kapibara-Internal-Token": "loadtest_token"}
        self.password = "testpassword"

    @task
    def register(self):
        external_user_uid, username, email = users_pool.get_register_details()
        result = self.client.post(
            "/v1/users/",
            json=dict(
                username=username,
                external_user_uid=external_user_uid,
                email=email,
                is_active=True,
            ),
            headers=self.internal_access_headers,
        )
        print(f"[{result.status_code}][REGISTER] Username: {username}")

        if result.status_code == 0:
            return
        users_pool.add_to_registered_user(username)
        
    @task
    def login(self):
        username = users_pool.get_registered_user()
        if not username:
            return
        
        result = self.client.post(
            "/v1/token/",
            json=dict(username=username, password=self.password),
            headers=self.internal_access_headers,
        )
        print(f"[{result.status_code}][LOGIN] Username: {username}")
        if result.status_code == 0:
            return
        data = result.json()
        token = data["access"]
        users_pool.add_to_tokens(token)

    @task
    def create_post(self):
        token = users_pool.get_token()
        if not token:
            return
        result = self.client.post(
            "/v1/posts/my/",
            json=dict(
                title=f"[LOADTEST] post id {uuid.uuid4()}",
                content=[{"type": "post"}],
                tags=[
                    "test_tag",
                    "test_one-more",
                    f"test_{random.randint(1, 100000000)}",
                ],
            ),
            headers={"Authorization": f"Bearer {token}"},
        )
        if result.status_code == 0:
            return
        
        data = result.json()
        uid = data["uuid"]
        print(f"[{result.status_code}][POST] uuid: {uid}")
        users_pool.add_to_post_uid_tokens((token, uid))

    @task
    def publish_post(self):
        token, uid = users_pool.get_post_uid_token()
        if not token or not uid:
            return
        result = self.client.post(
            f"/v1/posts/my/{uid}/publish/",
            headers={"Authorization": f"Bearer {token}"},
            name="/v1/posts/my/{uid}/publish/",
        )
        print(f"[{result.status_code}][POST PUBLISH] uuid: {uid}")
        users_pool.add_to_tokens(token)
        users_pool.add_to_post_uids(uid)

class VoteForPosts(SequentialTaskSet):
    def on_start(self):
        self.internal_access_headers = {"X-Kapibara-Internal-Token": "loadtest_token"}
        self.password = "testpassword"

    @task
    def vote_for_posts(self):
        token = users_pool.get_token()
        uids = users_pool.get_post_uids()
        if not token or not uids:
            return
        
        uids = uids.copy()

        print(f"[VOTING] Casting vote for {len(uids)} posts")
        def concurrent_voting(uid):
            with self.client.post(
                f"/v1/posts/{uid}/vote/",
                headers={"Authorization": f"Bearer {token}"},
                data={
                    "value": random.choice([1, -1])
                },
                name=f"/v1/posts/{len(uids)}/vote/",
                catch_response=True
            ) as result:
                print(f"[{result.status_code}][POST VOTE] uuid: {uid}")
                if result.status_code == 400:
                    result.success()

        pool = gevent.pool.Pool()
        for uid in uids:
            pool.spawn(concurrent_voting, uid)
        pool.join()




class LoadTestUser(FastHttpUser):
    tasks = {RegisterAndPublishPost: 10, VoteForPosts: 1}
    host = "http://localhost:8888"
    wait_time = constant_throughput(0.1)
